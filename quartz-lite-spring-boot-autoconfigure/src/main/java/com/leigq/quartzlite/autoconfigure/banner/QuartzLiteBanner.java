package com.leigq.quartzlite.autoconfigure.banner;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Banner
 *
 * @author leigq
 * @date 2020/11/4 16:06
 */
@Component
public class QuartzLiteBanner {
	/**
	 * Print banner.
	 */
	public void printBanner() {
		try (final InputStream is = this.getClass().getResourceAsStream("/quartz-lite-banner.txt")){
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = reader.readLine()) != null) {
				String coloredLine = line.replace("${GREEN}", "\u001B[32m").replace("${RESET}", "\u001B[0m");
				System.out.println(coloredLine);
			}
		} catch (IOException e) {
			// ignore
		}
	}
}
